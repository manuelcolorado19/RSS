require 'launchy'

class Newsletter 
    
  def check_all_news digg, reddit, mashable
    todo=[]
    (digg.organized_news.size-1).times do |i|
      todo.push(digg.organized_news[i])
    end
    (mashable.form_new_hash.size-1).times do |i|
      todo.push(mashable.form_new_hash[i])
    end
    (reddit.get_ordered_news.size-1).times do |i|
      todo.push(reddit.get_ordered_news[i])
    end
    m=todo.sort_by{ |x| x[2]}.reverse
  end

  def fetch_news m
    0.upto(m.size-1) do |i|
      m[i].unshift("noticia # #{i}")
    end
  end

  def open_browser option_user, all_news
    j="noticia # #{option_user}"
    all_news.size.times do |i|
      if all_news[i][0]==j
        puts "abriendo enlace"
        sleep 2
        Launchy.open(all_news[i][4])
      end
    end
  end

  def paginate_news all_news
    start=0 
    last=3 
    noticias=4
    salir="no"
    paginas= all_news.size/noticias
    until salir == "si"
      noticias%all_news.size!=0?paginas=(all_news.size/noticias)+1:paginas=all_news.size/noticias
      start.upto(last) do |i|
        puts all_news[i][0]
        puts "Titulo: #{all_news[i][1]}" 
        puts "Autor: #{all_news[i][2]}" 
        puts "Fecha: #{all_news[i][3]}"
        puts "URL: #{all_news[i][4]}"  
        puts "-----------------------------------"
      end
      puts "Hay #{paginas} páginas y estas en la página #{last/4+1}"
      puts "Ingresa el # de página que deseas ver, presiona 'e' para salir u 'o' para ver una noticia en tu browser"
      last=gets.chomp
      until last.to_i<=paginas
        puts "ingresa un número de página válido"
        last=gets.chomp
      end
      if last=="e"
        last=0
        salir="si"
      elsif last=="o"
        until last !="o"
          puts "ingresa la noticia que quieres abrir en tu browser"
          option_user=gets.chomp
          open_browser option_user, all_news
          puts "Ingresa el # de página que deseas ver,  presiona 'e' para salir u 'o' para ver una noticia en tu browser"
          last=gets.chomp
        end     
      else
          last=last.to_i
      end
      last=last.to_i
      if noticias%all_news.size!=0 && last==paginas
        total=all_news.size-1 
        actual=noticias*last
        aux=actual-total
        last=(noticias*last)-aux
        start=last-(noticias-aux)        
      else
        last= noticias*last-1
        start=last-noticias+1
      end
        system("clear")
    end    
  end 
end 
