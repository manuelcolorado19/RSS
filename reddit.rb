require "rest-client"
require "json"
require_relative 'newsletter'

class Reddit < Newsletter 
  attr_accessor :name
  def initialize
    url='https://www.reddit.com/.json'
    @response=RestClient.get(url)
    @hash= JSON.parse(get_body)
  end 

  def get_body 
    @response.body
  end 

  def get_ordered_news
    noticia=[]
    pote=0
    @hash["data"]["children"].each do |reddit|
      noticia[pote]=[]
      noticia[pote][0]=reddit["data"]["title"]
      noticia[pote][1]=reddit["data"]["author"]
      noticia[pote][2]=Time.at(reddit["data"]["created"]).strftime("%d-%m-%Y")
      noticia[pote][3]=reddit["data"]["url"]
      pote+=1
    end
    m=noticia.sort_by{ |x| x[2]}
    ordered=m.reverse
  end 
end 
