require "rest-client"
require "json"
require 'date'
require 'launchy'
require_relative 'newsletter'

class Digg < Newsletter
  attr_accessor :name
  def initialize
    url='http://digg.com/api/news/popular.json'
    @response= RestClient.get(url)
  end

  def get_body
    @response.body
  end

  def organized_news
    noticia=[]
    pote=0 
    hash=JSON.parse(get_body)
    hash["data"]["feed"].each do |new| 
      noticia[pote]=[]
      noticia[pote][0]=new["content"]["title"]
      noticia[pote][1]=new["content"]["author"]
      noticia[pote][2]=Time.at(new["date"]).strftime("%d-%m-%Y")
      noticia[pote][3]=new["content"]["original_url"]   
      pote+=1
    end
    ordered=noticia.sort_by{|x| x[2] }.reverse
  end
end 
