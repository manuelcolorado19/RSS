require "rest-client"
require "json"
require 'date'
require_relative 'newsletter'

class Mashable < Newsletter

  def initialize
    url = 'http://mashable.com/stories.json' 
    @response = RestClient.get(url)
    @hash= JSON.parse(get_request)
  end 
  
  def get_request
    @response.body 
  end

  def form_new_hash 
    noticia=[]
    pote=0   
    @hash["rising"].each do |new|
      noticia[pote]=[]
      noticia[pote][0]=new["title"]
      noticia[pote][1]=new["author"]
      aux=Date.parse(new["post_date"])
      noticia[pote][2]=aux.strftime("%d-%m-%Y")
      noticia[pote][3]=new["link"]
      pote+=1
    end 
    ordered=noticia.sort_by { |x| x[2] }.reverse
  end 
end 
